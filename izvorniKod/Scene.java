package raytracing;

/**
 * <p>Title: Scene</p>
 * <p>Description: </p>
 * Klasa predstvlja scenu kod modela crtanja slike pomocu ray tracinga. Sastoji
 * se od izvora svjetlosti i konacnog broja objekata.
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * @author Milenka Gadze, Miran Mosmondor
 * @version 1.1
 */


public class Scene {

    final int MAXDEPTH = 5; //maksimalna dubina rekurzije
    private int numberOfObjects;
    private Sphere[] sphere;
    private Point lightPosition;
    private ColorVector backroundColors = new ColorVector(0, 0, 0);
    private ColorVector light = new ColorVector((float) 1, (float) 1, (float) 1);
    private ColorVector ambientLight = new ColorVector((float) 0.5, (float) 0.5, (float) 0.5);

    /**
     * Inicijalni konstruktor koji postavlja poziciju svijetla i parametre svih
     * objekata u sceni.
     *
     * @param lightPosition    pozicija svijetla
     * @param numberOfObjects  broj objekata u sceni
     * @param sphereParameters parametri svih kugli
     */
    public Scene(Point lightPosition, int numberOfObjects, SphereParameters[] sphereParameters) {
        this.lightPosition = lightPosition;
        this.numberOfObjects = numberOfObjects;
        sphere = new Sphere[numberOfObjects];
        for (int i = 0; i < numberOfObjects; i++) {
            sphere[i] = new Sphere(sphereParameters[i]);
        }
    }

    /**
     * Metoda provjerava da li postoji sjena na tocki presjeka. Vraca true ako
     * se zraka od mjesta presjeka prema izvoru svjetlosti sjece s nekim objektom.
     *
     * @param intersection tocka presjeka
     * @return true ako postoji sjena u tocki presjeka, false ako ne postoji
     */
    private boolean shadow(Point intersection) {
        // zraka sjene - od presjeka do pozicije izvora
        Ray shadowRay = new Ray(intersection, lightPosition);

        // za svaki objekt ispitati postoji li presjek sa zrakom sjene
        // ako postoji -> true (promatrani objekt je u sjeni)
        for (int i = 0; i < numberOfObjects; i++) {
            Sphere curSphere = sphere[i];
            if (curSphere.intersection(shadowRay)) {
                return true;
            }
        }

        // nije pronaden presjek zrake za ispitivanje sjene ni s jednim objektom -> false (promatrani objekt nije u sjeni)
        return false;
    }

    /**
     * Metoda koja pomocu pracenja zrake racuna boju u tocki presjeka. Racuna se
     * osvjetljenje u tocki presjeka te se zbraja s doprinosima osvjetljenja koje
     * donosi reflektirana i refraktirana zraka.
     *
     * @param ray   pracena zraka
     * @param depth dubina rekurzije
     * @return vektor boje u tocki presjeka
     */
    public ColorVector traceRay(Ray ray, int depth) {
        // preslo max dubinu rekurzije -> vrati crnu boju
        if (depth > MAXDEPTH) {
            return new ColorVector(0, 0, 0);

        }

        // hvatiste zrake
        Point rayStartPnt = ray.getStartingPoint();
        // minDist - varijabla u koju pohranjujemo trenutno najmanju distancu do nekog presjeka (inicijalno +beskonacno)
        double minDist = Double.POSITIVE_INFINITY;
        // closestIntersectionPnt  - varijabla u koju cemo upisati tocku najblizeg presjeka
        Point closestIntersectionPnt = null;
        // objNum - varijabla u koju cemo upisati objekt do kojeg je trenutno najmanja distanca
        int objNum = -1;
        // za svaki objekt ispitati presjek sa zrakom
        for (int i = 0; i < numberOfObjects; i++) {
            // trenutni objekt (kugla)
            Sphere curSphere = sphere[i];

            // presjek postoji (true) ili ne (false)
            boolean intersectExists = curSphere.intersection(ray);
            // ako ne postoji presjek s trenutnim objektom, trazi presjek s drugim objektima redom
            if (!intersectExists)
                continue;

            // presjek postoji -> dohvati tocku presjeka zapisanu metodom intersection() klase Sphere() pozvanom gore
            Point intersectPnt = curSphere.getIntersectionPoint();

            // udaljenost trenutnog nadenog presjeka od hvatista zrake
            double curDist = rayStartPnt.getDistanceFrom(intersectPnt);
            // ako je udaljenost trenutnog nadenog presjeka od hvatista zrake veca od distance do dosad najblizeg pronadenog presjeka:
            // postavi novu minimalnu udaljenost na trenutnu (minDist), zabiljezi koordinate tog presjeka (closestIntersectionPnt) i redni broj trenutnog objekta (objNum)
            if (curDist < minDist) {
                minDist = curDist;
                closestIntersectionPnt = intersectPnt;
                objNum = i;
            }
        }

        // nema presjeka -> vrati boju pozadine
        if (closestIntersectionPnt == null)
            return backroundColors;

        // trenutna kugla
        Sphere curSphere = sphere[objNum];
        // izracun ambijentne komponente osvjetljenja kao Ia * ka (Ia - intenzitet ambijentnog osvjetljenja, ka - ambijentni koeficijent materijala trenutne kugle)
        ColorVector ambientComp = new ColorVector(0, 0, 0);
        ambientComp = ambientComp.add(ambientLight.multiple(curSphere.getKa()));
        // primijeni korekciju tako da RGB komponente ambijentnog osvjetljenja dodju u interval [0, 1]
        ambientComp.correct();

        // izracun vektora normale N (vektor od centra trenutne kugle do pronadenog presjeka zrake s kuglom), uz normalizaciju
        Vector vectorN = curSphere.getNormal(closestIntersectionPnt);
        vectorN.normalize();

        // izracun vektora L (vektor od pronadenog presjeka zrake s kuglom do pozicije izvora), uz normalizaciju
        Vector vectorL = new Vector(closestIntersectionPnt, lightPosition);
        vectorL.normalize();

        // izracun vektora V (vektor suprotan vektoru zrake), uz normalizaciju
        Vector vectorV = ray.getDirection().multiple(-1);
        vectorV.normalize();

        // ako je skalarani umnozak vektora V (suprotan vektor zraci) i N (normala) manji od 0 tj. kut izmedu njih je > 90
        // treba okrenuti smjer normale tako da pokazuje na stranu odakle dolazi zraka te ga normalizirati
        if (vectorV.dotProduct(vectorN) < 0) {
            vectorN = vectorN.multiple(-1);
            vectorN.normalize();
        }

        // R - reflektirani vektor vektora L (od presjeka prema izvoru) obzirom na normalu N, uz normalizaciju
        Vector vectorR = vectorL.getReflectedVector(vectorN);
        vectorR.normalize();


        // skalarni produkt vektora L (od presjeka prema izvoru) i N (normala)
        double dotLN = vectorL.dotProduct(vectorN);
        ColorVector diffuseComp = new ColorVector(0, 0, 0);
        // izracun difuzne komponente osvjetljenja kao Ii * kd * L*N (Ii - intenzitet izvora, kd - difuzni koeficijent materijala trenutne kugle, L*N - skalarni produkt vektora prema izvoru i normale)
        diffuseComp = diffuseComp.add(light.multiple(curSphere.getKd()).multiple(dotLN));
        // primijena korekciju tako da RGB komponente difuznog osvjetljenja dodju u interval [0, 1]
        diffuseComp.correct();

        // skalarni produkt vektora R (reflektirani vektor od L preko N) i N (normala)
        double dotRV = vectorR.dotProduct(vectorV);
        // skalarni produkt R*V podignut na potenciju parametra n koji određuje sjajnost materijala
        double dotRVToNPower = Math.pow(dotRV, curSphere.getN());
        ColorVector specularComp = new ColorVector(0, 0, 0);
        // izracun spekularne komponente osvjetljenja kao Ii * ks * (R*V)^n (Ii - intenzitet izvora, ks - spekularni koeficijent materijala trenutne kugle, (R*V)^n - skalarni produkt reflektiranog vektora i vektora prema ocistu na potenciju parametra sjajnosti n)
        specularComp = specularComp.add(light.multiple(curSphere.getKs()).multiple(dotRVToNPower));
        // primijena korekcije tako da RGB komponente spekularnog osvjetljenja dodju u interval [0, 1]
        specularComp.correct();

        // cLocal - lokalno osvjetljenje (ambijentno + difuzno + spekularno)
        ColorVector cLocal = new ColorVector(0, 0, 0);
        // pribrajanje ambijentne komponente - uvijek prisutna u lokalnom osvjetljenju
        cLocal = cLocal.add(ambientComp);

        // ispitivanje je li objekt u sjeni
        boolean isInShadow = shadow(closestIntersectionPnt);
        // ako je skalarani produkt vektora prema izvoru (L) i normale (N) veci od 0 (kut manji od 90 stupnjeva) i objekt nije u sjeni,
        // pribroji difuznu komponentu lokalnom osvjetljenju
        if (vectorL.dotProduct(vectorN) > 0 && !isInShadow) {
            cLocal = cLocal.add(diffuseComp);
        }
        // ako je skalarani produkt reflektiranog vektora (R) i vektora prema ocistu (V) veci od 0 (kut manji od 90 stupnjeva) i objekt nije u sjeni,
        // pribroji spekularnu komponentu lokalnom osvjetljenju
        if (vectorR.dotProduct(vectorV) > 0 && !isInShadow) {
            cLocal = cLocal.add(specularComp);
        }

        // primijena korekcije tako da RGB komponente lokalnog osvjetljenja dodju u interval [0, 1]
        cLocal.correct();


        // primjena utjecaja refleksije (odbijanja) svjetlosti

        // vectorReflected - refleksija vektora prema ocistu (V) obzirom na normalu (N)
        Vector vectorReflected = vectorV.getReflectedVector(vectorN);
        // provesti ray tracing od tocke najblizeg presjeka u smjeru reflektirane zrake na jednoj dubini rekurzije vise
        ColorVector cRefl = traceRay(new Ray(closestIntersectionPnt, vectorReflected), depth + 1);
        // dobiveni intenzitet reflektirane zrake dovesti u [0, 1]
        cRefl.correct();

        // primjena utjecaja refrakcije (loma) svjetlosti
        float ni = curSphere.getNi();
        // ako je skalarni produkt vektora prema ocistu (V) i normale (N) manji od 0 tj. kut je manji od 90 stupnjeva, data treba obrnuti omjer n1 i n2
        // odnosno koeficijent refrakcije postaviti na njegovu reciprocnu vrijednost
        if (vectorV.dotProduct(vectorN) < 0) {
            ni = 1 / ni;
        }
        // vectorRefracted - refrakcija vektora prema ocistu (V) na granici sredstava indeksa loma ni
        Vector vectorRefracted = vectorV.getRefractedVector(vectorN, ni);
        // provesti ray tracing od tocke najblizeg presjeka u smjeru refraktirane zrake na jednoj dubini rekurzije vise
        ColorVector cRefr = traceRay(new Ray(closestIntersectionPnt, vectorRefracted), depth + 1);
        // dobiveni intenzitet refraktirane zrake dovesti u [0, 1]
        cRefr.correct();

        // cResult - vektor intenziteta ukupnog osvjetljenja
        ColorVector cResult = new ColorVector(0, 0, 0);
        // pribrajanje intenziteta lokalnog osvjetljenja ukupnom
        cResult = cResult.add(cLocal);
        // pribrajanje intenziteta reflektirane znake pomnozene s koeficijentom refleksije kugle
        cResult = cResult.add(cRefl.multiple(curSphere.getReflectionFactor()));
        // pribrajanje intenziteta refraktirane znake pomnozene s koeficijentom refleksije kugle
        cResult = cResult.add(cRefr.multiple(curSphere.getRefractionFactor()));
        // korekcija konacnog intenziteta ukupnog osvjetljenja na [0, 1]
        cResult.correct();

        return cResult;
    }
}