package raytracing;

/**
 * <p>Title: Sphere</p>
 * <p>Description: </p>
 * Klasa predstavlja kuglu u prostoru. Nasljeduje apstraktnu klasu Object. Kugla
 * je odredena svojim polozajem, radijusom, bojom, parametrima materijala i
 * udjelima pojedninih zraka (osnovne, odbijene i lomljene).
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * @author Milenka Gadze, Miran Mosmondor
 * @version 1.1
 */


public class Sphere extends Object {

    private double radius;
    final double Epsilon = 0.0001;
    private double rayDistanceFromCenter;
    private Point IntersectionPoint;

    /**
     * Inicijalni konstruktor koji postavlja sve parametre kugle. Za prijenos
     * parametara koristi se pomocna klasa SphereParameters.
     *
     * @param sphereParameters parametri kugle
     */
    public Sphere(SphereParameters sphereParameters) {
        super(sphereParameters.getCenterPosition(), sphereParameters.getRaysContributions(), sphereParameters.getMaterialParameters(), sphereParameters.getN(), sphereParameters.getNi());
        this.radius = sphereParameters.getRadius();
    }

    /**
     * Metoda ispituje postojanje presjeka zrake ray s kuglom. Ako postoji presjek
     * postavlja atribut - tocku presjeka IntersectionPoint, te
     * vraca logicku vrijednost true.
     *
     * @param ray zraka za koju se ispituje postojanje presjeka sa kuglom
     * @return logicka vrijednost postojanja presjeka zrake s kuglom
     */
    public boolean intersection(Ray ray) {
        // PC - vektor od pocetne tocke zrake do centra kugle
        Vector PC = new Vector(ray.getStartingPoint(), centerPosition);

        // alpha - kut izmedu vektora PC i vektora zrake ray
        double alphaAngle = ray.getDirection().getAngle(PC);
        // ako je kut alpha veci od 90 stupnjeva (izrazen u radijanima pa prvo pretvaramo u stupnjeve), onda zraka ne sijece kuglu -> presjek ne postoji (false)
        if (180 / Math.PI * alphaAngle > 90)
            return false;

        // rayDistanceFromCenter - udaljenost d od zrake do sredusta kugle
        rayDistanceFromCenter = PC.getLength() * Math.sin(alphaAngle);
        // ako je udaljenost zrake do sredista kugle (rayDistanceFromCenter) veca od radijus kugle ->  presjek ne postoji (false)
        if (rayDistanceFromCenter > radius)
            return false;

        // distPD - udaljenost tocke P pocetka zrake ray do tocke D (tocka zrake najbliza centru kugle)
        double distPD = Math.sqrt(Math.pow(PC.getLength(), 2) - Math.pow(rayDistanceFromCenter, 2));
        // distPCloser - udaljenost tocke P do blizeg presjeka (-> oduzimanje)
        double distPCloser = distPD - Math.sqrt(Math.pow(radius, 2) - Math.pow(rayDistanceFromCenter, 2));
        // ako je distPCloser manje od predoredenog praga Epsilon -> pocetna tocka P zrake ray je unutar kugle
        if (distPCloser <= Epsilon) {
            distPCloser = distPD + Math.sqrt(Math.pow(radius, 2) - Math.pow(rayDistanceFromCenter, 2));
        }

        // tocka presjeka - od pocetne tocke zrake P (startingPoint) u njenom smjeru (direction) udaljena za izracunatu udaljenost distPCloser (udaljenost od pocetne tocke zrake P do blizeg presjeka zrake s kuglom)
        IntersectionPoint = new Point(ray.getStartingPoint(), ray.getDirection(), distPCloser);


        return true;
    }

    /**
     * Vraca tocku presjeka kugle sa zrakom koja je bliza pocetnoj tocki zrake.
     *
     * @return tocka presjeka zrake s kuglom koja je bliza izvoru zrake
     */
    public Point getIntersectionPoint() {
        return IntersectionPoint;
    }

    /**
     * Vraca normalu na kugli u tocki point
     *
     * @param point tocka kugle na kojoj se racuna normala na kugli
     * @return normal vektor normale
     */
    public Vector getNormal(Point point) {
        // normala je dogovorno usmjerena od centra kugle prema danoj tocki kugle
        return new Vector(centerPosition, point);
    }

    public double getRadius() {
        return radius;
    }
}