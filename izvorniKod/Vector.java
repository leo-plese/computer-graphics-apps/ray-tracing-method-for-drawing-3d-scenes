package raytracing;

/**
 * <p>Title: Vector</p>
 * <p>Description: </p>
 * Klasa predstavlja vektor u trodimenzionalnom prostoru.
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * @author Milenka Gadze, Miran Mosmondor
 * @version 1.1
 */

public class Vector {

    private double x, y, z;

    /**
     * Glavni konstruktor koji stvara vektor s komponentama x,y i z.
     *
     * @param x komponenta vektora
     * @param y komponenta vektora
     * @param z komponenta vektora
     */
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Konstruktor koji stvara vektor odreden dvijema tockama. Tocka first
     * predstavlja hvatiste vektora, a tocka second vrh vektora.
     *
     * @param first  tocka koja predstavlja pocetak, odnosno hvatiste vektora
     * @param second tocka koja zadaje vrh vektora
     */
    public Vector(Point first, Point second) {
        x = second.getX() - first.getX();
        y = second.getY() - first.getY();
        z = second.getZ() - first.getZ();
    }

    /**
     * Vraca x komponentu vektora.
     *
     * @return x komponenta vektora
     */
    public double getX() {
        return x;
    }

    /**
     * Vraca y komponentu vektora.
     *
     * @return y komponenta vektora
     */
    public double getY() {
        return y;
    }

    /**
     * Vraca z komponentu vektora.
     *
     * @return z komponenta vektora
     */
    public double getZ() {
        return z;
    }

    /**
     * Metoda normalizira vektor, odnosno stvara jedinicni vektor.
     */
    public void normalize() {
        double length = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        x /= length;
        y /= length;
        z /= length;
    }

    /**
     * Metoda vraca duzinu vektora.
     *
     * @return duzina vektora
     */
    public double getLength() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * Metoda sluzi za oduzimanje dva vektora.
     *
     * @param v vektor za koji se oduzima
     * @return vektor koji je jednak razlici
     */
    public Vector sub(Vector v) {
        return new Vector(x - v.getX(), y - v.getY(), z - v.getZ());
    }

    /**
     * Metoda sluzi za zbrajanje dva vektora.
     *
     * @param v vektor s kojim se zbraja
     * @return vektor koji je jednak zbroju
     */
    public Vector add(Vector v) {
        return new Vector(x + v.getX(), y + v.getY(), z + v.getZ());
    }

    /**
     * Metoda sluzi za mnozenje vektora skalarom.
     *
     * @param factor skalar s kojim se mnozi vektor
     * @return vektor koji je jednak umnosku vektora s skalarom
     */
    public Vector multiple(double factor) {
        return new Vector(x * factor, y * factor, z * factor);
    }

    /**
     * Koristi se za racunanje skalarnog produkta izmedu dva vektora.
     *
     * @param v vektor s kojim se racuna skalarni produkt
     * @return skalarni produkt dva vektora
     */
    public double dotProduct(Vector v) {
        return ((x * v.getX()) + (y * v.getY()) + (z * v.getZ()));
    }

    /**
     * Koristi se za racunanje vektorskog produkta.
     *
     * @param v vektor s kojim se racuna produkt
     * @return vektorski produkt dva vektora
     */
    public Vector crossProduct(Vector v) {
        double tx = (y * v.getZ()) - (v.getY() * z);
        double ty = (z * v.getX()) - (v.getZ() * x);
        double tz = (x * v.getY()) - (v.getX() * y);
        return new Vector(tx, ty, tz);
    }

    /**
     * Metoda vraca kut u radijanima (od 0 do PI) izmedu doticnog vektora i
     * vektora v.
     *
     * @param v vektor na odnosu kojeg se odreduje kut
     * @return kut izmedu dva vektora (u radijanima od 0 do PI)
     */
    public double getAngle(Vector v) {
        return Math.acos(this.dotProduct(v) / (this.getLength() * v.getLength()));
    }

    /**
     * Vraca reflektirani vektor s obzirom na normalu.
     *
     * @param normal normala
     * @return reflektirani vektor
     */
    public Vector getReflectedVector(Vector normal) {
        // izracun skalarnog produkta vektora L (od presjeka do izvora - to je ovaj vektor (this)) i N (normala u presjeku)
        double dotLN = this.dotProduct(normal);
        // izracun reflektiranog vektora formulom: (2*L*N)*N-L
        Vector vectorR = normal.multiple(2 * dotLN).sub(this);

        return vectorR;
    }

    /**
     * Vraca refraktirani vektor s obzirom na normalu i indekse refrakcije
     * sredstva upadnog vektora i refraktiranog vektora.
     *
     * @param normal normala
     * @param nI     indeks loma sredstva
     * @return refraktirani vektor
     */
    public Vector getRefractedVector(Vector normal, double nI) {
        // alpha - upadni kut s obzirom na normalu
        double alpha = this.getAngle(normal);
        double cosAlpha = Math.cos(alpha);

        // iz cinjenice R = -a*N - b*L tj. da je reflektirani vektor R linearna kombinacija vektora L i N
        // treba doznati koji su to tocno koeficijenti a i b da bi se tocno odredio R vektor

        // b = nI - koeficijent kojim se mnozi vektor L pri izracunu vektora R; indeks loma (omjer indeksa refrakcije n1 i n2 ulaznog odnosno izlaznog sredstva)
        double b = nI;

        // D je izraz koji se koristi u izracunu koeficijenta a
        double D = 4 * (Math.pow(b, 2) * Math.pow(cosAlpha, 2) - Math.pow(b, 2) + 1);
        // a - koeficijent kojim se mnozi vektor N pri izracunu vektora R;
        double a = (-2 * b * cosAlpha + Math.sqrt(D)) / 2;

        // konacan izracun reflektiranog vektora R gornjom formulom
        Vector vectorR = normal.multiple(-a).add(this.multiple(-b));

        return vectorR;
    }

}