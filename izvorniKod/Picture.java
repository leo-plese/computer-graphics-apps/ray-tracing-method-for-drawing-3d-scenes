package raytracing;

/**
 * <p>Title: Picture</p>
 * <p>Description: </p
 * Klasa koja stvara realisticnu sliku pomocu ray tracinga. Svi potrebni
 * parametri unose se pomocu datoteke Input.txt.
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * @author Milenka Gadze, Miran Mosmondor
 * @version 1.1
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Objects;


public class Picture extends JFrame {

    private int screenResolution;
    private Scene scene;
    private Screen screen;
    private Ray ray;
    private Point eyePosition;

    private BufferedImage img;

    /**
     * Konstruktor koji pomocu klase FileReader iz datoteke Input.txt uzima i
     * postavlja sve potebne parametre za crtanje slike : poziciju oka, rezoluciju
     * ekrana (slike), velicinu ekrana, poziciju svijetla u sceni, broj objekata u
     * sceni, te parametre svih kugli u sceni.
     */
    public Picture() {
        FileReader fileReader = new FileReader("input.txt");
        fileReader.read();

        eyePosition = fileReader.getEyePosition();
        screenResolution = fileReader.getScreenResolution();
        screen = new Screen(fileReader.getScreenSize(), screenResolution);
        scene = new Scene(fileReader.getLightPosition(), fileReader.getNumberOfObjects(), fileReader.getSphereParameters());

        //izlaz iz programa kada se zatvori prozor
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });


        // poboljsanje: slika se prvo kreira - izracunavaju se intenziteti RGB tj. boje svakog piksla i zatim se sprema u objekt BufferedImage
        // a tek na kraju se iscrtava metodom paint (dolje)
        img = new BufferedImage(screenResolution, screenResolution, BufferedImage.TYPE_INT_RGB);

        // postavljanje boje piksela za svaku poziciju (i, j) piksela u rezoluciji ekrana (ista sirina i visina = screenResolution)
        for (int j = 0; j < screenResolution; j++) {
            for (int i = 0; i < screenResolution; i++) {
                //postavljanje boje piksela na poziciji (i, j)

                // dobivanje tocke ekrana
                Point screenPoint = screen.getPoint(i, j);
                // dobivanje zrake od ocista do gore dobivene tocke ekrana
                ray = new Ray(eyePosition, screenPoint);

                // metodom ray tracing (s pocetnom dubinom rekurzije depth=1) dobiva se trazeni intenzitet trenutnog piksela
                ColorVector colors = scene.traceRay(ray, 1);
                Color c = new Color(colors.getRed(), colors.getGreen(), colors.getBlue());
                // u sliku tipa BufferedImage postavlja se da piksel (i, j) ima gore izracunatu boju
                img.setRGB(i, j, c.getRGB());
            }
        }

    }

    /**
     * Metoda sluzi za crtanje slike. Za svaku tocku slike (ekrana) racuna
     * boju pomocu ray tracinga.
     * <p>
     * Napomena: Ova metoda je ubrzana verzija prijasnje iste metode koja koristi sliku s vec izracunatim svim intenzitetima piksela u kontruktoru ove klase.
     *
     * @param g objekt tipa Graphics koji ce iscrtati prije zapamcenu slika tipa BufferedImage s postavljenim bojama svih piksela ekrana
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.drawImage(img, 0, 0, this);
        g2d.dispose();
    }


}