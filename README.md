# Ray Tracing Method for Drawing 3D Scenes

Implemented in Java.

Source code is in "izvorniKod" folder and executable code (.jar file) in "izvrsniKod" folder.

Virtual scene configuration parameters are in "input.txt" file. Note: name of this file must be same name which is used to initialize FileReader object in constructor of class "Picture" in "Picture.java".

Output pictures produced by ray tracing method are in "slike" folder.

Results are listed and described in "ResultsReport.pdf".

My lab assignment in Introduction to Virtual Environments, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2020